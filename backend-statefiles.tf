terraform {
  required_version = "~>1.5.2"
  backend "gcs" {
    credentials = "./creds/questradeuser.json"
    bucket      = "questrade-bucket"
  }
}