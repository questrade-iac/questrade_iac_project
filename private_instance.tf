resource "google_compute_instance" "private_instance" {
  name         = "private-instance"
  machine_type = var.instance_machine_type
  zone         = var.instance_zone
  boot_disk {
    initialize_params {
      image = var.instance_image
    }
  }
  network_interface {
    network    = google_compute_network.vpc.name
    subnetwork = google_compute_subnetwork.private_subnet.name
    access_config {
      # No external IP address
    }
  }
}