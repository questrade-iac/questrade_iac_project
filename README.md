# Secure Infrastructure Deployment on Google Cloud Platform (GCP) using Terraform and GitLab CI

This project aims to deploy a secure infrastructure on Google Cloud Platform (GCP) using Terraform and automate the deployment process with GitLab CI. The infrastructure consists of a Virtual Private Cloud (VPC) with a private and public subnets, along with two Compute Engine instances—one in the public subnet and another in the private subnet. Nginx is installed and running on the Compute Engine instance in the public subnet, serving a static HTML page. The Compute Engine instance in the private subnet should not have direct internet
access.

## Prerequisites
Before you begin, ensure that you have the following:

- A Google Cloud Platform (GCP) account.
- Create an IAM USER and KEY
- A GitLab account and access to the GitLab CI/CD.


## Getting Started
To get started with the project, follow these steps:

1. Clone this repository to your local machine.

git clone <repository_url>
cd <repository_name>

## Set up the project variables:
Open the variables.tf file and review the default values.
Modify the variables if needed to match your project requirements.

## Store your GCP service account credentials:
Copy your GCP service account credentials JSON file to a secure location, in this case i used GitLab CI/CD Variables
 - Go to your project in GitLab and navigate to Settings > CI/CD > Variables.
 - Create a new variable with an appropriate name (e.g., QUESTRADEUSER).
 - Paste the content of the service account JSON key file into the variable value field or (encode it as base64. OPTIPNAL BUT THAT WAS WHAT I DID FOR EXTRA SECURITY)
 - Ensure that the variable is marked as protected and masked (if it meets the masking requirements) to keep it secure.
In your CI/CD pipeline, you can then retrieve the variable and use it to authenticate with the necessary services.
Open the .gitlab-ci.yml file and replace <path/to/service_account.json> with the actual path to your service account credentials file.


## Configure GitLab CI/CD:
Open the .gitlab-ci.yml file and review the pipeline stages and jobs.
Customize the file if necessary to fit your deployment needs.
Commit and push the changes to your GitLab repository.

## Deploying the Infrastructure:
The infrastructure deployment is automated using GitLab CI/CD. Any commits pushed to the main branch will trigger the pipeline.

The pipeline consists of 4 stages:

Lint: This stage ensures that the Terraform configuration files have correct syntax and formatting.

Validate: This command is used to validate the syntax and configuration of your Terraform files without actually creating or modifying any resources. It checks for errors in the configuration files and ensures that the resource definitions are valid.

Plan: This command is used to create an execution plan for your Terraform infrastructure.

Apply: This stage provisions the infrastructure on GCP using Terraform. It validates, plans, and applies the changes. In this case i set it to apply manually just so i can review the plan before applying it, this can also be taken out to deploy automatically by deleting (When: manual) or adding what ever action you want.
       I also added an output.tf file with only one output which shows the public instance_external_ip.


## To monitor the pipeline and view the deployment progress:
Go to your GitLab repository.
Navigate to the "CI/CD" section or the "Pipelines" tab.
Select the pipeline corresponding to your latest commit.
Review the pipeline logs and status to monitor the deployment.

## Accessing the Infrastructure
Once the pipeline successfully deploys the infrastructure, you can access the components as follows:

The Compute Engine instance in the public subnet running Nginx should be accessible over the internet. Use the public IP address assigned to the instance to access the static HTML page.

The Compute Engine instance in the private subnet does not have direct internet access. You can access it via other instances within the VPC or through bastion host configurations.

## Cleaning Up

To remove the deployed infrastructure and clean up resources:
Go to your GitLab repository.
Navigate to the "CI/CD" section or the "Pipelines" tab.
Find the pipeline associated with the deployment.
Click on the "Revert" button to trigger a Terraform destroy operation.
Confirm the destroy operation to remove the infrastructure.

or

You can add a Terraform destroy stage:

 #This file performs the destruction of the infrastructure provisioned by Terraform
 destroy:
   stage: destroy
   script:
   - terraform destroy -auto-approve
   dependencies:
   - apply
   when: manual
Please note that the destroy operation is irreversible and will delete all the resources provisioned by Terraform

