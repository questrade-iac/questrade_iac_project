provider "google" {
  credentials = file("./creds/questradeuser.json")
  project     = var.project_id
  region      = var.region
}