variable "project_id" {
  description = "Google Cloud project ID"
  type        = string
  default     = "questrade-project"
}

variable "region" {
  description = "GCP region"
  type        = string
  default     = "us-central1"
}

variable "public_subnet_cidr" {
  description = "CIDR block for the public subnet"
  type        = string
  default     = "10.0.1.0/24"
}

variable "private_subnet_cidr" {
  description = "CIDR block for the private subnet"
  type        = string
  default     = "10.0.2.0/24"
}

variable "instance_machine_type" {
  description = "Machine type for the compute instance"
  type        = string
  default     = "n1-standard-1"
}

variable "instance_zone" {
  description = "Zone for the compute instance"
  type        = string
  default     = "us-central1-c"
}

variable "instance_image" {
  description = "Image for the compute instance"
  type        = string
  default     = "ubuntu-os-cloud/ubuntu-2004-lts"
}