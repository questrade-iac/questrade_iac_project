resource "google_compute_instance" "public_instance" {
  name         = "public-instance"
  machine_type = var.instance_machine_type
  zone         = var.instance_zone
  boot_disk {
    initialize_params {
      image = var.instance_image
    }
  }
  network_interface {
    network    = google_compute_network.vpc.name
    subnetwork = google_compute_subnetwork.public_subnet.name
    access_config {}
  }
  metadata_startup_script = "apt-get update && apt-get install -y nginx && echo 'HI MY NAME IS CHIJIOKE KEVIN EZE I WOULD BE SO DELIGHTED IF I AM OFFERED THIS ROLE' > /var/www/html/index.html"

  tags = ["http-ssh"] # I added a tag to the instance for the firewall rule
}