resource "google_compute_network" "vpc" {
  name                    = "my-vpc"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "public_subnet" {
  name          = "public-subnet"
  network       = google_compute_network.vpc.name
  ip_cidr_range = var.public_subnet_cidr
}

resource "google_compute_subnetwork" "private_subnet" {
  name          = "private-subnet"
  network       = google_compute_network.vpc.name
  ip_cidr_range = var.private_subnet_cidr
}

resource "google_compute_firewall" "public_instance_firewall" {
  name          = "public-instance-firewall"
  network       = google_compute_network.vpc.name
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "tcp"
    ports    = ["80", "22"]
  }

  target_tags = google_compute_instance.public_instance.tags
}